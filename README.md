# Ragged Adwaita

This is a modified Adwaita theme that has OSX-like window controls. Regular SSD
(server side decoration) windows also have thinner titlebars.

**Important disclaimer**: I don't own **any** of the icons in this repo, they
were straight out copied from another theme which I can't remember (if you are
the owner please let me know so I can give you credit).

## Screenshots

![Screenshot 1](https://imgur.com/f20hXS4.png)

![Screenshot 2](https://imgur.com/fd3mRWk.png)

## Credits

This theme (apart from being 99.999% default Adwaita) is a modified version of
the theme provided in this video https://www.youtube.com/watch?v=cdKTi5R0k-M.